import numpy as np
import pandas as pd
from pandas.core.frame import DataFrame

def compute_score(Y_train, Y_proposed) : 
    """ 
    Y_train: np.array or dataframe from QRT, size 200.000 * 1
    Y_proposed: proposed dataset, same size as Y_train
    """
    if isinstance(Y_proposed, pd.DataFrame) :
        Y_proposed = Y_proposed['RET_TARGET'].values

    if isinstance(Y_train, pd.DataFrame):
        Y_train = Y_train['RET_TARGET'].values

    temp = (np.sign(Y_train) == np.sign(Y_proposed)) * Y_train
    return np.linalg.norm(temp, ord = 1) / np.linalg.norm(Y_train, ord = 1)

def compute_accuracy(Y_train, Y_proposed):
    if isinstance(Y_proposed, pd.DataFrame) :
        Y_proposed = Y_proposed['RET_TARGET'].values
        
    if isinstance(Y_train, pd.DataFrame):
        Y_train = Y_train['RET_TARGET'].values

    temp = (np.sign(Y_train) == np.sign(Y_proposed)) 
    return temp.sum()/len(Y_train)

def make_list_unique(L) : 
    res = [L[0]]
    for i in range(len(L)-1):
        if L[i+1] != L[i] : 
            res.append(L[i + 1])
    return res

def proprecess_XY_clean(df):
    return df.values[:, 1:]

def proprecess_X_csv(df):
    return df.groupby(['ID_DAY']).mean().values[:, 1:-1]

def process_output(output, DF, Y_KEYS) : 


    RET_hash_map = {}
    for i, e in enumerate(Y_KEYS): 
        RET_hash_map[e] = i

    day_a = np.min(DF['ID_DAY'])
    day_z = np.max(DF['ID_DAY'])
    n_days = day_z - day_a + 1

    assert n_days == len(DF.groupby('ID_DAY').mean()) 

    day_list = make_list_unique(DF['ID_DAY'])
    ordered_day_list = sorted(day_list)

    day_hash_map = {}
    for d1, d2 in zip(ordered_day_list, day_list) : 
        day_hash_map[d2] = d1

    unsorted_res = []

    for i, day_id in enumerate(ordered_day_list) : 
        target_id_for_day_i = DF[DF['ID_DAY'] == day_id]['ID_TARGET']
        target_idx = [RET_hash_map[id] for id in target_id_for_day_i]
        good_output_for_day_i = output[i][target_idx]
        unsorted_res.append((day_id, good_output_for_day_i))

    sorted_res = sorted(unsorted_res, key = lambda e : day_hash_map[e[0]])
    res = np.empty((len(DF), ))

    i = 0 
    sliding_index = 0
    while i < n_days:
        vect = sorted_res[i][1]
        res[sliding_index : sliding_index + len(vect)] = vect
        sliding_index += len(vect)
        i += 1

    return res

def to_dataframe(y): 
    Y_df = pd.DataFrame(y, columns=['RET_TARGET'])
    Y_df.index += 267100
    return Y_df