from utils import make_list_unique
from load_data import X_TRAIN, Y_KEYS
import numpy as np 


DF = X_TRAIN
day_a = np.min(X_TRAIN['ID_DAY'])
day_z = np.max(X_TRAIN['ID_DAY'])
n_days = day_z - day_a + 1

output = np.random.random((n_days, 100))

def proprecess_X_csv(df):
    return df.groupby(['ID_DAY']).mean().values[:, 1:-1]


x = proprecess_X_csv(X_TRAIN)


hash_map = {}
for i, e in enumerate(Y_KEYS): 
    hash_map[e] = i


assert n_days == len(DF.groupby('ID_DAY').mean()) 


day_list = make_list_unique(DF['ID_DAY'])
ordered_day_list = sorted(day_list)

day_hash_map = {}
for d1, d2 in zip(ordered_day_list, day_list) : 
    day_hash_map[d1] = d2

unsorted_res = []

for i, day_id in enumerate(ordered_day_list) : 
    target_id_for_day_i = DF[DF['ID_DAY'] == day_id]['ID_TARGET']
    target_idx = [hash_map[id] for id in target_id_for_day_i]
    good_output_for_day_i = output[i][target_idx]
    unsorted_res.append((day_id, good_output_for_day_i))

sorted_res = sorted(unsorted_res, key = lambda e : day_hash_map[e[0]])
res = np.empty((267100, ))

i = 0 
sliding_index = 0
while i < n_days:
    vect = sorted_res[i][1]
    res[sliding_index : sliding_index + len(vect)] = vect
    sliding_index += len(vect)
    i += 1

return res