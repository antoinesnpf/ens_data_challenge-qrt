import os
import pandas as pd
from cfg import DATAFOLDER

X_TRAIN = pd.read_csv(os.path.join(DATAFOLDER, 'X_train.csv'))
Y_TRAIN = pd.read_csv(os.path.join(DATAFOLDER,'y_train.csv'), index_col=0)
X_TEST = pd.read_csv(os.path.join(DATAFOLDER,'X_test.csv'))
X = pd.read_csv(os.path.join(DATAFOLDER,'X.csv'))
X.pop('Unnamed: 0')
Y = pd.read_csv(os.path.join(DATAFOLDER,'Y.csv'))
Y.pop('Unnamed: 0')

Y_KEYS = list(int(u) for u in Y.keys()[1:])


