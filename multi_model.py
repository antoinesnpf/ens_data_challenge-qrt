import random as rd
import numpy as np 
import os
import pandas as pd
import time


from cfg import RESULTSFOLDER
from utils import to_dataframe, process_output, compute_accuracy, compute_score, proprecess_XY_clean, proprecess_X_csv


t = time.time()
from load_data import X, Y, X_TEST, X_TRAIN, Y_TRAIN, Y_KEYS
x = proprecess_XY_clean(X)
y = proprecess_XY_clean(Y)

m = []
names =  os.listdir(os.path.join(RESULTSFOLDER, "à mixer")) 



jacques_df = names[0]
jacques_df = pd.read_csv(os.path.join(RESULTSFOLDER, "à mixer", jacques_df))
del jacques_df['Unnamed: 0']
jacques_df = process_output(jacques_df.values, X_TEST, Y_KEYS)


antoine_df_1 = names[1]
antoine_df_1 = pd.read_csv(os.path.join(RESULTSFOLDER, "à mixer", antoine_df_1))
del antoine_df_1['Unnamed: 0']

antoine_df_2 = names[2]
antoine_df_2 = pd.read_csv(os.path.join(RESULTSFOLDER, "à mixer", antoine_df_2))
del antoine_df_2['Unnamed: 0']

res_df = to_dataframe(0.65 * jacques_df + 0.08 * antoine_df_1.values[:,0] + 0.27 * antoine_df_2.values[:,0])
res_df.to_csv('BGSUMBIT.csv')