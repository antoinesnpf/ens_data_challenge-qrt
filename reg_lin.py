import random as rd
import numpy as np 
import os
import pandas as pd

from sklearn.impute import SimpleImputer, KNNImputer
from sklearn.linear_model import Ridge, Lasso, ElasticNet, MultiTaskLasso, MultiTaskElasticNet
from sklearn.preprocessing import StandardScaler, PolynomialFeatures, SplineTransformer, Normalizer
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import make_pipeline
from sklearn import decomposition
import time

from cfg import RESULTSFOLDER
from utils import to_dataframe, process_output, compute_accuracy, compute_score, proprecess_XY_clean, proprecess_X_csv


# read https://www.kdnuggets.com/2019/07/data-pre-processing-optimizing-regression-model-performance.html

def save(Y_df, savename, score, accuracy) : 
    'Y_df: pandas DataFrame'

    assert savename.endswith('.csv')
    assert Y_df.size == 114468

    overwritting = os.path.exists(os.path.join(RESULTSFOLDER, savename))
    Y_df.to_csv(os.path.join(RESULTSFOLDER, savename))

    txt_savename = savename[:-4] + '.txt'
    with open(os.path.join(RESULTSFOLDER, txt_savename), 'w') as file: 
        file.write(f'score: {score}')
        file.write(f'accuracy: {accuracy}')

    if overwritting : 
        print(f"file saved with overwritting of {savename}")
    else :
        print(f"saved as {savename}")


if __name__ == '__main__': 
    #Seeding
    np.random.seed(999)

    # Preprocessing parameters
    nan_mode = 'none'
    normalization_mode = 'none'

    # Model choice
    imputer = KNNImputer(n_neighbors = 5, weights = 'uniform')
    scaler = StandardScaler()
    y_scaler = StandardScaler()
    non_linear_model = SplineTransformer(degree = 3, n_knots = 5)
    reg_model =  MultiTaskLasso()

    # Grid search parameters
    parameters = {
        'multitasklasso__alpha' : [0.02154434], 
        'splinetransformer__degree' : [2],
        'splinetransformer__n_knots' : [3]
    }

    # Saving parameters
    saving = True
    savename = 'knnImputer15__Scaler__MultiTaskLasso__Spline_2.csv'

    #Loading data
    t = time.time()
    from load_data import X, Y, X_TEST, X_TRAIN, Y_TRAIN, Y_KEYS
    x = proprecess_XY_clean(X)
    y = proprecess_XY_clean(Y)
    y = y_scaler.fit_transform(imputer.fit_transform(y))
    x_train = proprecess_X_csv(X_TRAIN)
    x_test = proprecess_X_csv(X_TEST)
    print(f'Loading time: {time.time() - t}')
    
    # Training
    t = time.time()
    pipeline = make_pipeline(imputer, scaler, non_linear_model, reg_model)
    model = GridSearchCV(pipeline, parameters, n_jobs = -1)
    model.fit(x, y)
    print(f'Training time: {time.time() - t}')

    # Predictions
    t = time.time()
    y_out_train_split = process_output(y_scaler.inverse_transform(model.predict(x_train)), X_TRAIN, Y_KEYS)
    if saving : y_out_test_split = process_output(y_scaler.inverse_transform(model.predict(x_test)), X_TEST, Y_KEYS)
    print(f'Testing time: {time.time() - t}')  

    # Saving and scoring
    score = compute_score(Y_TRAIN, y_out_train_split)
    accuracy = compute_accuracy(Y_TRAIN, y_out_train_split)
    print('score:', score)
    print('accuracy:', accuracy)

    if saving :
        save(to_dataframe(y_out_test_split), savename, score, accuracy)


