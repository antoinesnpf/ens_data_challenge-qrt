import numpy as np
import pandas as pd
import copy
import os
from cfg import DATAFOLDER

X_train = pd.read_csv(os.path.join(DATAFOLDER, 'X_train.csv'))
Y_train = pd.read_csv(os.path.join(DATAFOLDER,'y_train.csv'), index_col=0)

target_idxs = np.array([u for u in X_train['ID_TARGET']])
inputs_idxs = np.array([int(u[4:]) for u in (X_train.keys()) if 'RET' in u])

target_ids = np.array(['RET_' + str(idx) for idx in target_idxs])
inputs_ids = np.array(['RET_' + str(idx) for idx in inputs_idxs])


# Define X and Y, consize view of the data
day_a = np.min(X_train['ID_DAY'])
day_z = np.max(X_train['ID_DAY'])
n = day_z - day_a + 1
init = np.empty((n, ))
init[:] = np.nan

Y = {'day' : copy.copy(init)}    
X = {'day' : copy.copy(init)}
for target_idx in target_idxs :
    Y[target_idx] = copy.copy(init)
for inputs_idx in inputs_idxs :
    X[inputs_idx] = copy.copy(init)


for day in range(day_a, day_z + 1):
    i = day - day_a
    Y['day'][i] = int(day - day_a)
    X['day'][i] = int(day - day_a)
    
    mask = (X_train['ID_DAY'] == day)
    temp = X_train[mask]
    for id, row in temp.iterrows() : 
        id
        target_id = int(row['ID_TARGET'])
        Y[target_id][i] = (Y_train.loc[id, 'RET_TARGET'])

    for input_idx in inputs_idxs:
        X[input_idx][i] = row['RET_' + str(input_idx)]


X = pd.DataFrame(X)
Y = pd.DataFrame(Y)

X.to_csv(os.path.join(DATAFOLDER, 'X.csv'))
Y.to_csv(os.path.join(DATAFOLDER, 'Y.csv'))

del X
del Y 
del X_train
del Y_train

